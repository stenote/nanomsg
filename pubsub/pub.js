var nano = require('nanomsg')

var pub = nano.socket('pub')
var addr = 'tcp://127.0.0.1:8899'

pub.bind(addr)
let i = 0;
let v = 0;
setInterval(() => {
	i ++;
	v = i % 2;
	console.log(`${v}|hello from nanomsg`)
	pub.send(`${v}|Hello from nanomsg`)
}, 1000)


pub.on('data', function(buf) {
   console.log(buf.toString())
});

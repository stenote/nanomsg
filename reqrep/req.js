var nano = require('nanomsg');

var req = nano.socket('req');

var addr = 'tcp://127.0.0.1:5555';

req.bind(addr);

req.on('data', function (buf) {
	  console.log('received response: ', buf.toString());
});

let i = 0;
setInterval(function() {
 i ++;
 req.send(i);

}, 100)

var nano = require('nanomsg')

var bus = nano.socket('bus')
var addr = 'tcp://127.0.0.1:8899'

bus.bind(addr)

bus.on('data', function(buf) {
  console.log(buf.toString())
  bus.send(buf.toString())
});

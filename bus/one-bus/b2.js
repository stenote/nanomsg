var nano = require('nanomsg')

var bus = nano.socket('bus')
var addr = 'tcp://127.0.0.1:8899'

bus.connect(addr)

bus.on('data', function(buf) {
	console.log(buf.toString())
})

setInterval(function() {
  bus.send('b2')
}, 1000);

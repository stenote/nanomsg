var nano = require('nanomsg')

var bus = nano.socket('bus')
var addr = 'tcp://127.0.0.1:33333'

bus.bind(addr)
bus.connect('tcp://127.0.0.1:33331')

bus.on('data', function(buf) {
  console.log(buf.toString())
});

setInterval(function() {
  bus.send('b3')
}, 100);
